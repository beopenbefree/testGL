/*
 * display.h
 *
 *  Created on: Jan 6, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_DISPLAY_H_
#define INCLUDE_DISPLAY_H_

#include "utilities.h"

void display(GLFWwindow*, double);

#endif /* INCLUDE_DISPLAY_H_ */
