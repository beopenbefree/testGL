/*
 * Sphere.h
 *
 *  Created on: Jan 17, 2021
 *      Author: beopenbefree
 */

#ifndef SRC_SPHERE_H_
#define SRC_SPHERE_H_

#include <vector>
#include <glm/glm.hpp>

using namespace std;

namespace utils
{

class Sphere final
{
public:
	Sphere();
	Sphere(int prec);
	virtual ~Sphere();
	inline int getNumVertices() const;
	inline int getNumIndices() const;
	inline const vector<int>& getIndices() const;
	inline const vector<glm::vec3>& getVertices() const;
	inline const vector<glm::vec2>& getTexCoords() const;
	inline const vector<glm::vec3>& getNormals() const;

private:
	int numVertices = 0;
	int numIndices = 0;
	vector<int> indices;
	vector<glm::vec3> vertices;
	vector<glm::vec2> texCoords;
	vector<glm::vec3> normals;
	void init(int);

};

inline int Sphere::getNumVertices() const
{
	return numVertices;
}

inline int Sphere::getNumIndices() const
{
	return numIndices;
}

inline const vector<int>& Sphere::getIndices() const
{
	return indices;
}

inline const vector<glm::vec3>& Sphere::getVertices() const
{
	return vertices;
}

inline const vector<glm::vec2>& Sphere::getTexCoords() const
{
	return texCoords;
}

inline const vector<glm::vec3>& Sphere::getNormals() const
{
	return normals;
}

} /* namespace utils */

#endif /* SRC_SPHERE_H_ */
