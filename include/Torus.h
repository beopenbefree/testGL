/*
 * Torus.h
 *
 *  Created on: Jan 17, 2021
 *      Author: beopenbefree
 */

#ifndef SRC_TORUS_H_
#define SRC_TORUS_H_

#include <vector>
#include <glm/glm.hpp>

using namespace std;

namespace utils
{

class Torus final
{
public:
	Torus();
	Torus(float innerRadius, float outerRadius, int prec);
	virtual ~Torus();
	inline const std::vector<int>& getIndices() const;
	inline const std::vector<glm::vec3>& getNormals() const;
	inline int getNumIndices() const;
	inline int getNumVertices() const;
	inline const std::vector<glm::vec3>& getSTangents() const;
	inline const std::vector<glm::vec2>& getTexCoords() const;
	inline const std::vector<glm::vec3>& getTTangents() const;
	inline const std::vector<glm::vec3>& getVertices() const;

private:
	int numVertices = 0;
	int numIndices = 0;
	int prec = 0;
	float inner = 0;
	float outer = 0;
	std::vector<int> indices;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> texCoords;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec3> sTangents;
	std::vector<glm::vec3> tTangents;
	void init();

};

inline const std::vector<int>& Torus::getIndices() const
{
	return indices;
}

inline const std::vector<glm::vec3>& Torus::getNormals() const
{
	return normals;
}

inline int Torus::getNumIndices() const
{
	return numIndices;
}

inline int Torus::getNumVertices() const
{
	return numVertices;
}

inline const std::vector<glm::vec3>& Torus::getSTangents() const
{
	return sTangents;
}

inline const std::vector<glm::vec2>& Torus::getTexCoords() const
{
	return texCoords;
}

inline const std::vector<glm::vec3>& Torus::getTTangents() const
{
	return tTangents;
}

inline const std::vector<glm::vec3>& Torus::getVertices() const
{
	return vertices;
}

} /* namespace utils */

#endif /* SRC_TORUS_H_ */
