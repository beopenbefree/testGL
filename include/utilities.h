/**
 * utilities.h
 *
 *  Created on: Nov 28, 2020
 *      Author: consultit
 */

#ifndef INCLUDE_UTILITIES_H_
#define INCLUDE_UTILITIES_H_

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <string>
#include <fstream>
#include <SOIL2.h>

using namespace std;

namespace utils
{

// opengl/glsl error log
void printShaderLog(GLuint);
void printProgramLog(GLuint);
bool checkOpenGLError();

// glsl source file read
string readShaderSource(const string&);

// shader program
GLuint createShaderProgram(const string&, const string&);

// load texture
GLuint loadTexture(const string&);

} // end namespace utils

#endif /* INCLUDE_UTILITIES_H_ */
