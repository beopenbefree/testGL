/*
 * init.h
 *
 *  Created on: Jan 6, 2021
 *      Author: beopenbefree
 */

#ifndef INCLUDE_INIT_H_
#define INCLUDE_INIT_H_

#include "utilities.h"
#include <glm/glm.hpp>
#include "Sphere.h"
#include "Torus.h"
#include "ImportedModel.h"
#include <memory>

using namespace std;

constexpr GLuint numVAOs = 1;
constexpr GLuint numVBOs = 14;

extern glm::mat4 pMat;

extern GLuint vao[numVAOs];
extern GLuint vbo[numVBOs];

extern GLuint renderingProgram;

extern glm::vec3 cameraLoc;
extern float cameraYaw, cameraPitch, cameraRoll;
extern glm::vec3 sunLoc, sunRotAxis;
extern float sunRotFreq;
extern glm::vec3 planetLoc, planetRotAxis;
extern float planetRotFreq, planetRevFreq, planetRevX, planetRevZ;
extern glm::vec3 moonLoc, moonRotAxis;
extern float moonRotFreq, moonRevFreq, moonRevX, moonRevZ;

extern GLuint brickTexture;
extern utils::Sphere sphere;
extern utils::Torus torus;
extern shared_ptr<utils::ImportedModel> model;

void init(GLFWwindow*, const string&, const string&, const string&,
		const string&);

#endif /* INCLUDE_INIT_H_ */
