/*
 * ImportedModel.h
 *
 *  Created on: Jan 18, 2021
 *      Author: beopenbefree
 */

#ifndef SRC_IMPORTEDMODEL_H_
#define SRC_IMPORTEDMODEL_H_

#include <vector>
#include <string>
#include <glm/glm.hpp>

using namespace std;

namespace utils
{

// ------------ ImportedModel class
class ImportedModel final
{
public:
	ImportedModel(const string &filePath);
	virtual ~ImportedModel();
	inline const vector<glm::vec3>& getNormals() const;
	inline int getNumVertices() const;
	inline const vector<glm::vec2>& getTextureCoords() const;
	inline const vector<glm::vec3>& getVertices() const;

private:
	int numVertices;
	vector<glm::vec3> vertices;
	vector<glm::vec2> texCoords;
	vector<glm::vec3> normalVecs;

};

inline const vector<glm::vec3>& ImportedModel::getNormals() const
{
	return normalVecs;
}

inline int ImportedModel::getNumVertices() const
{
	return numVertices;
}

inline const vector<glm::vec2>& ImportedModel::getTextureCoords() const
{
	return texCoords;
}

inline const vector<glm::vec3>& ImportedModel::getVertices() const
{
	return vertices;
}

// -------------- ModelImporter class
class ModelImporter final
{
public:
	ModelImporter();
	virtual ~ModelImporter();
	inline const vector<float>& getNormals() const;
	inline const vector<float>& getTextureCoordinates() const;
	inline const vector<float>& getVertices() const;
	void parseOBJ(const string &filePath);
	inline int getNumVertices() const;

private:
	// values as read in from OBJ file
	vector<float> vertVals;
	vector<float> stVals;
	vector<float> normVals;
	// values stored for later use as vertex attributes
	vector<float> triangleVerts;
	vector<float> textureCoords;
	vector<float> normals;

};

inline const vector<float>& ModelImporter::getNormals() const
{
	return normals;
}

inline const vector<float>& ModelImporter::getTextureCoordinates() const
{
	return textureCoords;
}

inline const vector<float>& ModelImporter::getVertices() const
{
	return triangleVerts;
}

inline int ModelImporter::getNumVertices() const
{
	return triangleVerts.size() / 3;
}

} /* namespace utils */

#endif /* SRC_IMPORTEDMODEL_H_ */
