/*
 * init.cpp
 *
 *  Created on: Jan 6, 2021
 *      Author: beopenbefree
 */

#include "init.h"
#include <glm/gtc/matrix_transform.hpp>

GLuint vao[numVAOs];
GLuint vbo[numVBOs];

glm::mat4 pMat;

GLuint renderingProgram;

glm::vec3 cameraLoc;
float cameraYaw, cameraPitch, cameraRoll;
glm::vec3 sunLoc, sunRotAxis;
float sunRotFreq;
glm::vec3 planetLoc, planetRotAxis;
float planetRotFreq, planetRevFreq, planetRevX, planetRevZ;
glm::vec3 moonLoc, moonRotAxis;
float moonRotFreq, moonRevFreq, moonRevX, moonRevZ;

GLuint brickTexture;
utils::Sphere sphere;
utils::Torus torus;
shared_ptr<utils::ImportedModel> model;

namespace
{
void window_reshape_callback(GLFWwindow*, int, int);
void setupVertices(const string&);
}

void init(GLFWwindow *window, const string &vertShaderPath,
		const string &fragShaderPath, const string &texImagePath,
		const string &importedModel)
{
	// build perspective matrix
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	// 1.0472 radians = 60 degrees
	pMat = glm::perspective(1.0472f, /*aspect*/(float) width / (float) height,
			0.1f, 1000.0f);
	// set size window callback
	glfwSetWindowSizeCallback(window, window_reshape_callback);

	renderingProgram = utils::createShaderProgram(vertShaderPath,
			fragShaderPath);
	// camera
	// Global Euler rotations: applied with Y->P->R sequence
	// Local Euler rotations: applied with R->P->Y sequence
	// Yaw: rotateY; Pitch: rotateX; Roll: rotateZ
	cameraYaw = glm::radians(0.0);
	cameraPitch = glm::radians(-20.0);
	cameraRoll = glm::radians(0.0);
	cameraLoc = glm::vec3(0.0f, 5.0f, 17.0f);
	// sun
	sunLoc = glm::vec3(0.0f, 0.0f, 0.0f);
	sunRotAxis = glm::vec3(0.0f, 1.0f, 0.0f);
	sunRotFreq = 1.0;
	// planet
	planetRotAxis = glm::vec3(1.0f, 0.0f, 0.0f);
	planetRotFreq = 2.0;
	planetRevFreq = 0.25;
	planetRevX = 8.0;
	planetRevZ = 8.0;
	// moon
	moonRotAxis = glm::vec3(0.0f, 1.0f, 0.0f);
	moonRotFreq = 4.0;
	moonRevFreq = 6.0;
	moonRevX = 2.5;
	moonRevZ = 2.5;

	setupVertices(importedModel);

	// get texture object
	brickTexture = utils::loadTexture(texImagePath);
}

namespace
{
void window_reshape_callback(GLFWwindow *window, int newWidth, int newHeight)
{
	glViewport(0, 0, newWidth, newHeight);
	pMat = glm::perspective(1.0472f, /*aspect*/
	(float) newWidth / (float) newHeight, 0.1f, 1000.0f);
}

// cube
extern float cubePositions[108];
extern float cubeTexCoords[72];
// pyramid
extern float pyramidPositions[54];
extern float pyrTexCoords[36];

// setup geometry
void setupVertices(const string &importedModel)
{

	// we need at least 1 VAO
	glGenVertexArrays(numVAOs, vao);
	// we need at least numVBOs VBOs
	glGenVertexArrays(1, vao);
	glBindVertexArray(vao[0]);
	glGenBuffers(numVBOs, vbo);

	// CUBE
	// coords
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubePositions), cubePositions,
	GL_STATIC_DRAW);
	// texture coords
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTexCoords), cubeTexCoords,
	GL_STATIC_DRAW);

	// PYRAMID
	// coords
	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidPositions), pyramidPositions,
	GL_STATIC_DRAW);
	// texture coords
	glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyrTexCoords), pyrTexCoords,
	GL_STATIC_DRAW);

	// SPHERE
	{
		vector<int> ind = sphere.getIndices();
		vector<glm::vec3> vert = sphere.getVertices();
		vector<glm::vec2> tex = sphere.getTexCoords();
		vector<glm::vec3> norm = sphere.getNormals();
		vector<float> pvalues;	 // vertex positions
		vector<float> tvalues;	 // texture coordinates
		vector<float> nvalues;	 // normal vectors
		int numIndices = sphere.getNumIndices();
		for (int i = 0; i < numIndices; i++)
		{
			pvalues.push_back((vert[ind[i]]).x);
			pvalues.push_back((vert[ind[i]]).y);
			pvalues.push_back((vert[ind[i]]).z);

			tvalues.push_back((tex[ind[i]]).s);
			tvalues.push_back((tex[ind[i]]).t);

			nvalues.push_back((norm[ind[i]]).x);
			nvalues.push_back((norm[ind[i]]).y);
			nvalues.push_back((norm[ind[i]]).z);
		}

		// coords
		glBindBuffer(GL_ARRAY_BUFFER, vbo[4]);
		glBufferData(GL_ARRAY_BUFFER, pvalues.size() * 4, &pvalues[0],
		GL_STATIC_DRAW);
		// texture coords
		glBindBuffer(GL_ARRAY_BUFFER, vbo[5]);
		glBufferData(GL_ARRAY_BUFFER, tvalues.size() * 4, &tvalues[0],
		GL_STATIC_DRAW);
		// normal coords
		glBindBuffer(GL_ARRAY_BUFFER, vbo[6]);
		glBufferData(GL_ARRAY_BUFFER, nvalues.size() * 4, &nvalues[0],
		GL_STATIC_DRAW);
	}

	// TORUS
	{
		vector<int> ind = torus.getIndices();
		vector<glm::vec3> vert = torus.getVertices();
		vector<glm::vec2> tex = torus.getTexCoords();
		vector<glm::vec3> norm = torus.getNormals();
		vector<float> pvalues;	 // vertex positions
		vector<float> tvalues;	 // texture coordinates
		vector<float> nvalues;	 // normal vectors
		int numVertices = torus.getNumVertices();
		for (int i = 0; i < numVertices; i++)
		{
			pvalues.push_back(vert[i].x);
			pvalues.push_back(vert[i].y);
			pvalues.push_back(vert[i].z);

			tvalues.push_back(tex[i].s);
			tvalues.push_back(tex[i].t);

			nvalues.push_back(norm[i].x);
			nvalues.push_back(norm[i].y);
			nvalues.push_back(norm[i].z);
		}

		// coords
		glBindBuffer(GL_ARRAY_BUFFER, vbo[7]);
		glBufferData(GL_ARRAY_BUFFER, pvalues.size() * 4, &pvalues[0],
		GL_STATIC_DRAW);
		// texture coords
		glBindBuffer(GL_ARRAY_BUFFER, vbo[8]);
		glBufferData(GL_ARRAY_BUFFER, tvalues.size() * 4, &tvalues[0],
		GL_STATIC_DRAW);
		// normal coords
		glBindBuffer(GL_ARRAY_BUFFER, vbo[9]);
		glBufferData(GL_ARRAY_BUFFER, nvalues.size() * 4, &nvalues[0],
		GL_STATIC_DRAW);
		// indexes
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[10]); // indices
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, ind.size() * 4, ind.data(),
		GL_STATIC_DRAW);
	}

	// MODEL
	{
		model = shared_ptr<utils::ImportedModel>(
				new utils::ImportedModel(importedModel));
		vector<glm::vec3> vert = (*model).getVertices();
		vector<glm::vec2> tex = (*model).getTextureCoords();
		vector<glm::vec3> norm = (*model).getNormals();
		int numObjVertices = (*model).getNumVertices();
		vector<float> pvalues;
		// vertex positions
		vector<float> tvalues;
		// texture coordinates
		vector<float> nvalues;
		// normal vectors
		for (int i = 0; i < numObjVertices; i++)
		{
			pvalues.push_back((vert[i]).x);
			pvalues.push_back((vert[i]).y);
			pvalues.push_back((vert[i]).z);

			tvalues.push_back((tex[i]).s);
			tvalues.push_back((tex[i]).t);

			nvalues.push_back((norm[i]).x);
			nvalues.push_back((norm[i]).y);
			nvalues.push_back((norm[i]).z);
		}
		// VBO for vertex locations
		glBindBuffer(GL_ARRAY_BUFFER, vbo[11]);
		glBufferData(GL_ARRAY_BUFFER, pvalues.size() * 4, &pvalues[0],
		GL_STATIC_DRAW);
		// VBO for texture coordinates
		glBindBuffer(GL_ARRAY_BUFFER, vbo[12]);
		glBufferData(GL_ARRAY_BUFFER, tvalues.size() * 4, &tvalues[0],
		GL_STATIC_DRAW);
		// VBO for normal vectors
		glBindBuffer(GL_ARRAY_BUFFER, vbo[13]);
		glBufferData(GL_ARRAY_BUFFER, nvalues.size() * 4, &nvalues[0],
		GL_STATIC_DRAW);
	}
}

float cubePositions[108] =
{ -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f,
		-1.0f, // back face
		1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		-1.0f, // right face
		1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, // front face
		-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f,
		1.0f, // left face
		-1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
		1.0f, // bottom face
		-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, -1.0f // top face
		};

float cubeTexCoords[72] =
{ 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, // back face
		1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, // right face
		1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, // front face
		1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, // left face
		0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom face
		0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f // top face
		};

float pyramidPositions[54] =
{ -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, // front face
		1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, // right face
		1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f, // back face
		-1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, // left face
		-1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, // base – left front
		1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f	// base – right back
		};

float pyrTexCoords[36] =
{ 0.0f, 0.0f, 1.0f, 0.0f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.5f, 1.0f, // top and right faces
		0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.5f, 1.0f, // back and left faces
		0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f // base triangles
		};

}
