/**
 * main.cpp
 *
 *  Created on: Nov 27, 2020
 *      Author: consultit
 */

#include "utilities.h"
#include "init.h"
#include "display.h"

int main(int argc, char **argv)
{
	string vertShaderPath, fragShaderPath, texImagePath, importedModel;
	if (argc >= 5)
	{
		vertShaderPath = argv[1];
		fragShaderPath = argv[2];
		texImagePath = argv[3];
		importedModel = argv[4];
	}

	if (!glfwInit())
	{
		return EXIT_FAILURE;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	GLFWwindow *window = glfwCreateWindow(600, 600,
			"Computer Graphics Programming", nullptr, nullptr);

	glfwMakeContextCurrent(window);

	if (glewInit() != GLEW_OK)
	{
		return EXIT_FAILURE;
	}

	glfwSwapInterval(1);

	// call init()
	init(window, vertShaderPath, fragShaderPath, texImagePath, importedModel);

	// render loop
	while (!glfwWindowShouldClose(window))
	{
		// call display()
		display(window, glfwGetTime());
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);
	glfwTerminate();

	return EXIT_SUCCESS;
}
