/*
 * display.cpp
 *
 *  Created on: Jan 6, 2021
 *      Author: beopenbefree
 */

#include "init.h"
#include "display.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>

namespace
{
// allocate variables used in display() function, so that they won’t need to be allocated during rendering
GLuint mvLoc, projLoc;
glm::mat4 vMat;
std::stack<glm::mat4> mvStack;
}

void display(GLFWwindow *window, double currentTime)
{
	glClear(GL_DEPTH_BUFFER_BIT);
	glClear(GL_COLOR_BUFFER_BIT);
	glUseProgram(renderingProgram);

	// get & set the uniform variables for the projection matrix
	projLoc = glGetUniformLocation(renderingProgram, "proj_matrix");
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(pMat));

	// get the uniform variables for the MV matrix
	mvLoc = glGetUniformLocation(renderingProgram, "mv_matrix");

	// build and push view matrix onto the stack
	// We use local Euler rotations:
	// Transform = inverse((rotateZ->rotateX->rotateY)->translation) => inverse(Trans * RotY * RotX * RotZ)
	vMat = glm::translate(glm::mat4(1.0f), cameraLoc);
	vMat = glm::rotate(vMat, cameraYaw, glm::vec3(0, 1, 0));
	vMat = glm::rotate(vMat, cameraPitch, glm::vec3(1, 0, 0));
	vMat = glm::rotate(vMat, cameraRoll, glm::vec3(0, 0, 1));
	vMat = glm::inverse(vMat);
	mvStack.push(vMat);

	// draw geometry
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	{
		glFrontFace(GL_CCW);
		// ---------------------- model == sun --------------------------------------------
		mvStack.push(mvStack.top());
		// position
		mvStack.top() *= glm::translate(glm::mat4(1.0f), sunLoc);
		mvStack.push(mvStack.top());
		// rotation
		mvStack.top() *= glm::rotate(glm::mat4(1.0f),
				(float) currentTime * sunRotFreq, sunRotAxis);

		glUniformMatrix4fv(mvLoc, 1, GL_FALSE, glm::value_ptr(mvStack.top()));
		// vertex coords
		glBindBuffer(GL_ARRAY_BUFFER, vbo[11]);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);
		// texture coords
		glBindBuffer(GL_ARRAY_BUFFER, vbo[12]);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);
		// activate texture
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, brickTexture);

		// render state
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		// draw
		glDrawArrays(GL_TRIANGLES, 0, (*model).getNumVertices());
		// remove axial rotation
		mvStack.pop();
	}

	{
		glFrontFace(GL_CCW);
		// ---------------------- torus == planet --------------------------------------------
		mvStack.push(mvStack.top());
		// position
		float planetRevAngle = (float) currentTime * planetRevFreq;
		mvStack.top() *= glm::translate(glm::mat4(1.0f),
				glm::vec3(cos(planetRevAngle) * planetRevX, 0,
						-sin(planetRevAngle) * planetRevZ));
		mvStack.push(mvStack.top());
		// rotation
		mvStack.top() *= glm::rotate(glm::mat4(1.0f),
				(float) currentTime * planetRotFreq, planetRotAxis);

		glUniformMatrix4fv(mvLoc, 1, GL_FALSE, glm::value_ptr(mvStack.top()));
		// vertex coords
		glBindBuffer(GL_ARRAY_BUFFER, vbo[7]);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);
		// texture coords
		glBindBuffer(GL_ARRAY_BUFFER, vbo[8]);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);
		// activate texture
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, brickTexture);

		// render state
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		// draw
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[10]);
		glDrawElements(GL_TRIANGLES, torus.getNumIndices(), GL_UNSIGNED_INT, 0);

		// remove axial rotation
		mvStack.pop();
	}

	{
		glFrontFace(GL_CW);
		// ---------------------- smaller cube == moon --------------------------------------------
		mvStack.push(mvStack.top());
		// position
		float moonRevAngle = (float) currentTime * moonRevFreq;
		mvStack.top() *= glm::translate(glm::mat4(1.0f),
				glm::vec3(cos(-moonRevAngle) * moonRevX, 0,
						-sin(-moonRevAngle) * moonRevZ));
		mvStack.push(mvStack.top());
		// rotation
		mvStack.top() *= glm::rotate(glm::mat4(1.0f),
				(float) currentTime * moonRotFreq, moonRotAxis);
		// scale
		mvStack.top() *= glm::scale(glm::mat4(1.0f),
				glm::vec3(0.25, 0.25, 0.25));

		glUniformMatrix4fv(mvLoc, 1, GL_FALSE, glm::value_ptr(mvStack.top()));
		// vertex coords
		glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);
		// texture coords
		glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);
		// activate texture
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, brickTexture);

		// render state
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		// draw
		glDrawArrays(GL_TRIANGLES, 0, 36);
		// remove axial rotation
		mvStack.pop();
	}
	glDisable(GL_CULL_FACE);

	// cleanup
	while (!mvStack.empty())
	{
		mvStack.pop();
	}
	assert(mvStack.empty());
}

