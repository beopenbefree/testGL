/**
 * utilities.cpp
 *
 *  Created on: Nov 28, 2020
 *      Author: consultit
 */

#include "utilities.h"

namespace utils
{

void printShaderLog(GLuint shader)
{
	int len = 0;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
	if (len > 0)
	{
		int chWritten = 0;
		char *log = new char[len];
		glGetShaderInfoLog(shader, len, &chWritten, log);
		cout << "Shader Info Log: " << log << endl;
		delete[] log;
	}
}

void printProgramLog(GLuint prog)
{
	int len = 0;
	glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &len);
	if (len > 0)
	{
		int chWritten = 0;
		char *log = new char[len];
		glGetProgramInfoLog(prog, len, &chWritten, log);
		cout << "Program Info Log: " << log << endl;
		delete[] log;
	}
}

bool checkOpenGLError()
{
	bool foundError = false;
	GLenum glErr = glGetError();
	while (glErr != GL_NO_ERROR)
	{
		cout << "glError: " << glErr << endl;
		foundError = true;
		glErr = glGetError();
	}
	return foundError;
}

string readShaderSource(const string &filePath)
{
	string content;
	ifstream fileStream(filePath, ios::in);
	if (!fileStream.fail())
	{
		string line = "";
		while (!fileStream.eof())
		{
			getline(fileStream, line);
			content.append(line + "\n");
		}
	}
	fileStream.close();
	return content;
}

GLuint createShaderProgram(const string &vertShaderPath,
		const string &fragShaderPath)
{
	string vshaderStr = readShaderSource(vertShaderPath);
	string fshaderStr = readShaderSource(fragShaderPath);
	const char *vshaderSource = vshaderStr.c_str();
	const char *fshaderSource = fshaderStr.c_str();

	// vertex shader
	GLuint vShader = glCreateShader(GL_VERTEX_SHADER);
	GLint vertCompiled;
	glShaderSource(vShader, 1, &vshaderSource, nullptr);
	glCompileShader(vShader);
	checkOpenGLError();
	glGetShaderiv(vShader, GL_COMPILE_STATUS, &vertCompiled);
	if (vertCompiled != 1)
	{
		cout << "vertex shader compilation failed!" << endl;
		printShaderLog(vShader);
	}

	// fragment shader
	GLuint fShader = glCreateShader(GL_FRAGMENT_SHADER);
	GLint fragCompiled;
	glShaderSource(fShader, 1, &fshaderSource, nullptr);
	glCompileShader(fShader);
	checkOpenGLError();
	glGetShaderiv(fShader, GL_COMPILE_STATUS, &fragCompiled);
	if (fragCompiled != 1)
	{
		cout << "fragment shader compilation failed!" << endl;
		printShaderLog(fShader);
	}

	// shader program
	GLuint vfProgram = glCreateProgram();
	GLint linked;
	glAttachShader(vfProgram, vShader);
	glAttachShader(vfProgram, fShader);
	glLinkProgram(vfProgram);
	checkOpenGLError();
	glGetProgramiv(vfProgram, GL_LINK_STATUS, &linked);
	if (linked != 1)
	{
		cout << "shader program linking failed!" << endl;
		printProgramLog(vfProgram);
	}

	return vfProgram;
}

GLuint loadTexture(const string &texImagePath)
{
	GLuint textureID;
	textureID = SOIL_load_OGL_texture(texImagePath.c_str(), SOIL_LOAD_AUTO,
			SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	if (textureID == 0)
	{
		cout << "could not find texture file" << texImagePath << endl;
	}

	// if mipmapping
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
			GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	// if also anisotropic filtering
	if (glewIsSupported("GL_EXT_texture_filter_anisotropic"))
	{
		GLfloat anisoSetting = 0.0f;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &anisoSetting);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT,
				anisoSetting);
	}

	return textureID;

}

} // end namespace utils
