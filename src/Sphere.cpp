/*
 * Sphere.cpp
 *
 *  Created on: Jan 17, 2021
 *      Author: beopenbefree
 */

#include "Sphere.h"

namespace utils
{

Sphere::Sphere()
{
	init(48);
}

Sphere::Sphere(int prec)
{
	init(prec);
}

Sphere::~Sphere()
{
	// TODO Auto-generated destructor stub
}

void Sphere::init(int prec)
{
	numVertices = (prec + 1) * (prec + 1);
	numIndices = prec * prec * 6;
	vertices.resize(numVertices);
	texCoords.resize(numVertices);
	normals.resize(numVertices);
	indices.resize(numIndices);

	// calculate triangle vertices
	for (int i = 0; i <= prec; i++)
	{
		for (int j = 0; j <= prec; j++)
		{
			// up
			float y = glm::cos(glm::radians(180.0 - i * 180.0 / prec));
			// right, forward
			float x = -glm::cos(glm::radians(j * 360.0f / prec))
					* glm::abs(glm::cos(glm::asin(y)));
			float z = glm::sin(glm::radians(j * 360.0f / prec))
					* glm::abs(glm::cos(glm::asin(y)));
			// store in containers
			vertices[i * (prec + 1) + j] = glm::vec3(x, y, z);
			texCoords[i * (prec + 1) + j] = glm::vec2(((float) j / prec),
					((float) i / prec));
			normals[i * (prec + 1) + j] = glm::vec3(x, y, z);
		}
	}

	// calculate triangle indices
	for (int i = 0; i < prec; i++)
	{
		for (int j = 0; j < prec; j++)
		{
			indices[6 * (i * prec + j) + 0] = i * (prec + 1) + j;
			indices[6 * (i * prec + j) + 1] = i * (prec + 1) + j + 1;
			indices[6 * (i * prec + j) + 2] = (i + 1) * (prec + 1) + j;
			indices[6 * (i * prec + j) + 3] = i * (prec + 1) + j + 1;
			indices[6 * (i * prec + j) + 4] = (i + 1) * (prec + 1) + j + 1;
			indices[6 * (i * prec + j) + 5] = (i + 1) * (prec + 1) + j;
		}
	}

}

} /* namespace utils */
