/*
 * ImportedModel.cpp
 *
 *  Created on: Jan 18, 2021
 *      Author: beopenbefree
 */

#include "ImportedModel.h"
#include <fstream>
#include <sstream>

namespace utils
{

// ------------ ImportedModel class
ImportedModel::ImportedModel(const std::string &filePath)
{
	ModelImporter modelImporter = ModelImporter();
	modelImporter.parseOBJ(filePath);
	// uses modelImporter to get vertex information
	numVertices = modelImporter.getNumVertices();
	std::vector<float> verts = modelImporter.getVertices();
	std::vector<float> tcs = modelImporter.getTextureCoordinates();
	std::vector<float> normals = modelImporter.getNormals();
	for (int i = 0; i < numVertices; i++)
	{
		vertices.push_back(
				glm::vec3(verts[i * 3], verts[i * 3 + 1], verts[i * 3 + 2]));
		texCoords.push_back(glm::vec2(tcs[i * 2], tcs[i * 2 + 1]));
		normalVecs.push_back(
				glm::vec3(normals[i * 3], normals[i * 3 + 1],
						normals[i * 3 + 2]));
	}
}

ImportedModel::~ImportedModel()
{
	// TODO Auto-generated destructor stub
}

// -------------- ModelImporter class
ModelImporter::ModelImporter()
{
	// TODO Auto-generated constructor stub

}

ModelImporter::~ModelImporter()
{
	// TODO Auto-generated destructor stub
}

void ModelImporter::parseOBJ(const std::string &filePath)
{
	float x, y, z;
	string content;
	ifstream fileStream(filePath, ios::in);
	string line = "";
	while (!fileStream.eof())
	{
		getline(fileStream, line);
		// vertex position ("v" case)
		if (line.compare(0, 2, "v ") == 0)
		{
			stringstream ss(line.erase(0, 1));
			// extract the vertex position values
			ss >> x;
			ss >> y;
			ss >> z;
			vertVals.push_back(x);
			vertVals.push_back(y);
			vertVals.push_back(z);
		}
		// texture coordinates ("vt" case)
		if (line.compare(0, 2, "vt") == 0)
		{
			stringstream ss(line.erase(0, 2));
			// extract texture coordinate values
			ss >> x;
			ss >> y;
			stVals.push_back(x);
			stVals.push_back(y);
		}
		// vertex normals ("vn" case)
		if (line.compare(0, 2, "vn") == 0)
		{
			stringstream ss(line.erase(0, 2));
			// extract the normal vector values
			ss >> x;
			ss >> y;
			ss >> z;
			normVals.push_back(x);
			normVals.push_back(y);
			normVals.push_back(z);
		}
		// triangle faces ("f" case)
		if (line.compare(0, 2, "f ") == 0)
		{
			string oneCorner, v, t, n;
			stringstream ss(line.erase(0, 2));
			for (int i = 0; i < 3; i++)
			{
				// extract triangle face references
				getline(ss, oneCorner, ' ');
				stringstream oneCornerSS(oneCorner);
				getline(oneCornerSS, v, '/');
				getline(oneCornerSS, t, '/');
				getline(oneCornerSS, n, '/');

				// "stoi" converts string to integer
				int vertRef = (stoi(v) - 1) * 3;
				int tcRef = (stoi(t) - 1) * 2;
				int normRef = (stoi(n) - 1) * 3;

				// build vector of vertices
				triangleVerts.push_back(vertVals[vertRef]);
				triangleVerts.push_back(vertVals[vertRef + 1]);
				triangleVerts.push_back(vertVals[vertRef + 2]);

				// build vector of texture coords
				textureCoords.push_back(stVals[tcRef]);
				textureCoords.push_back(stVals[tcRef + 1]);

				//... and normals
				normals.push_back(normVals[normRef]);
				normals.push_back(normVals[normRef + 1]);
				normals.push_back(normVals[normRef + 2]);
			}
		}
	}
}

} /* namespace utils */
